package main.java.com.vega.ania;

import main.java.com.vega.ania.SQLite.Database;
import main.java.com.vega.ania.extro.ComponentsList;
import main.java.com.vega.ania.extro.DefaultImage;
import main.java.com.vega.ania.windows.addContainer.AddActivity;
import main.java.com.vega.ania.windows.mainContainer.MainActivity;
import main.java.com.vega.ania.windows.settingsContainer.SettingActivity;
import main.java.com.vega.ania.windows.updateContainer.UpdateActivity;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Вася-Вега on 16.11.2015.
 */
public class Main {

    public static final String PATHIMG = System.getProperty("user.dir") +"/images/";

    static {
        File imagePath = new File(PATHIMG);
        imagePath.mkdirs();
        Database.createDB();
        ResultSet resultSet = Database.readTable();
        try {
            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String title = resultSet.getString("title");
                String img = resultSet.getString("img");
                String text = resultSet.getString("text");
                String balls = resultSet.getString("balls");
                ImageIcon imageIcon = null;
                File file = new File(PATHIMG + img);
                if (file.length() > 0){
                    imageIcon = new ImageIcon(file.getAbsolutePath());
                }else {
                    imageIcon = DefaultImage.getImage();
                }
                ComponentsList.addItemList(id, title, imageIcon, text, balls);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static JFrame jFrame = new JFrame("Ania");
    static{
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setSize(800, 600);
        jFrame.getContentPane().setBackground(Color.darkGray);
        jFrame.setResizable(false);
        jFrame.setLocationRelativeTo(null);
        jFrame.add(new MainActivity());
    }

    public static JFrame addFrame = new JFrame("Ania");
    static {
        addFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addFrame.setSize(800, 600);
        addFrame.getContentPane().setBackground(Color.darkGray);
        addFrame.setResizable(false);
        addFrame.add(new AddActivity());
    }

    public static JFrame updateFrame = new JFrame("Ania");
    static {
        updateFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        updateFrame.setSize(800, 600);
        updateFrame.getContentPane().setBackground(Color.darkGray);
        updateFrame.setResizable(false);
        updateFrame.add(new UpdateActivity());
    }

    public static JFrame settingFrame = new JFrame("Ania");
    static {
        settingFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        settingFrame.setSize(800, 600);
        settingFrame.getContentPane().setBackground(Color.darkGray);
        settingFrame.setResizable(false);
        settingFrame.add(new SettingActivity());
    }

    public static void main(String[] args) {
        jFrame.setVisible(true);
    }

}
