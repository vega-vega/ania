package main.java.com.vega.ania.windows.updateContainer;

import main.java.com.vega.ania.extro.ComponentsList;
import main.java.com.vega.ania.Main;
import main.java.com.vega.ania.SQLite.Database;
import main.java.com.vega.ania.extro.ResizeImg;
import main.java.com.vega.ania.windows.addContainer.AddForm;
import main.java.com.vega.ania.windows.mainContainer.MainActivity;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Вася-Вега on 18.11.2015.
 */
public class UpdateButtons extends JPanel {

    private static int LOCATIONWIDTH = 120;
    private static int LOCATIONHEIGHT = 50;
    private static int WIDTH = 100;
    private static int HEIGHT = 30;
    private JLabel acceptLabel = new JLabel("Update accepted");
    private JLabel deniedLabel = new JLabel("Update denied");

    public UpdateButtons(int x, int y, int width, int height) {
        setBounds(x, y, width, height);
        LOCATIONHEIGHT = height / 12;
        LOCATIONWIDTH = width / 6;
        WIDTH = width / 2 + LOCATIONWIDTH;
        setOpaque(false);
        setLayout(null);

        JButton button = new JButton("Update");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 1);
        button.addActionListener(new UpdateButton());
        add(button);

        button = new JButton("Close");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 2);
        button.addActionListener(new CloseButton());
        add(button);


        acceptLabel.setSize(WIDTH, HEIGHT);
        acceptLabel.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 3);
        acceptLabel.setForeground(Color.green);
        acceptLabel.setFont(new Font(Font.SANS_SERIF, 3, 18));
        acceptLabel.setVisible(false);
        add(acceptLabel);

        deniedLabel.setSize(WIDTH, HEIGHT);
        deniedLabel.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 3);
        deniedLabel.setForeground(Color.red);
        deniedLabel.setFont(new Font(Font.SANS_SERIF, 3, 18));
        deniedLabel.setVisible(false);
        add(deniedLabel);
    }

    private class CloseButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Main.jFrame.setLocation(Main.updateFrame.getLocation());
            Main.updateFrame.setVisible(false);
            Main.jFrame.setVisible(true);
            acceptLabel.setVisible(false);
            deniedLabel.setVisible(false);
        }
    }

    private class UpdateButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String title = UpdateForm.titleText.getText();
            String img = null;
            if (UpdateForm.imgPath.getText().length() > 1) {
                img = ResizeImg.path(UpdateForm.imgPath.getText());
            }
            String text = UpdateForm.textArea.getText();
            int balls = Integer.parseInt(UpdateForm.balls.getSelectedItem().toString());
            if (title.length() > 0 && text.length() > 0) {

                Database.updatePost(title, img, text, balls, UpdateForm.id);
                ComponentsList.updateItem(UpdateForm.id, title, img,
                        text, String.valueOf(balls));
                Main.jFrame.getContentPane().removeAll();
                Main.jFrame.add(new MainActivity());
                deniedLabel.setVisible(false);
                acceptLabel.setVisible(true);
            }else {
                deniedLabel.setVisible(true);
                acceptLabel.setVisible(false);
            }
        }
    }
}
