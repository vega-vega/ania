package main.java.com.vega.ania.windows.updateContainer;

import main.java.com.vega.ania.extro.ComponentsList;
import main.java.com.vega.ania.extro.Setting;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by Вася-Вега on 18.11.2015.
 */
public class UpdateForm extends JPanel {

    static JFileChooser fileChooser;
    final static int HEIGHT = 25;
    final static int LEFTMARGIN = 50;
    final static int YLOCATION = 75;
    public static JTextField imgPath  = new JTextField();
    public static JTextField titleText = new JTextField();
    public static JTextArea textArea = new JTextArea();
    public static JComboBox balls = new JComboBox(Setting.getBalls());
    public static int id;

    static {
        FileFilter newFilter = new FileNameExtensionFilter("Image", "jpg", "png");
        fileChooser = new JFileChooser();
        FileFilter oldFilter = fileChooser.getFileFilter();
        fileChooser.removeChoosableFileFilter(oldFilter);
        fileChooser.setFileFilter(newFilter);
    }

    public UpdateForm(int x, int y, int width, int height) {
        setBounds(x, y, width, height);
        setOpaque(false);
        setLayout(null);
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(2, 5, 3, 5), null));

        imgPath.setSize(345, HEIGHT);
        imgPath.setLocation(LEFTMARGIN, YLOCATION);
        add(imgPath);

        JButton button = new JButton("path");
        button.setSize(80, HEIGHT);
        button.setLocation(400, YLOCATION);
        button.addActionListener(new PathButton());
        add(button);

        titleText.setSize(430, HEIGHT);
        titleText.setLocation(LEFTMARGIN, YLOCATION * 2);
        add(titleText);


        textArea.setSize(430, 180);
        textArea.setLocation(LEFTMARGIN, YLOCATION * 3);
        add(textArea);

        balls = new JComboBox(Setting.getBalls());
        balls.setEditable(true);
        balls.setSize(75, HEIGHT);
        balls.setLocation(LEFTMARGIN, YLOCATION * 6);
        add(balls);

        Font font = new Font(Font.SANS_SERIF, 3, 16);

        JLabel label = new JLabel("Path for image");
        label.setSize(430, HEIGHT);
        label.setLocation(LEFTMARGIN, (YLOCATION - 35));
        label.setForeground(Color.orange);
        label.setFont(font);
        add(label);

        label = new JLabel("Title post");
        label.setSize(430, HEIGHT);
        label.setLocation(LEFTMARGIN, YLOCATION * 2 - 35);
        label.setForeground(Color.orange);
        label.setFont(font);
        add(label);

        label = new JLabel("Text in post");
        label.setSize(430, HEIGHT);
        label.setLocation(LEFTMARGIN, YLOCATION * 3 - 35);
        label.setForeground(Color.orange);
        label.setFont(font);
        add(label);

        label = new JLabel("Balls of post");
        label.setSize(430, HEIGHT);
        label.setLocation(LEFTMARGIN, YLOCATION * 6 - 35);
        label.setForeground(Color.orange);
        label.setFont(font);
        add(label);

    }

    private class PathButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            int result = fileChooser.showDialog(null, "Open image");
            if (result == JFileChooser.APPROVE_OPTION){
                imgPath.setText(String.valueOf(fileChooser.getSelectedFile()));
            }
        }
    }

    public static void findItem(Integer ids){
        id = ids;
        java.util.List<Component> list = ComponentsList.getItem(id).getListItem();
        String title = Parse.title(list.get(0));
        String description = Parse.description(list.get(2));
        String ball = Parse.balls(list.get(3));
        titleText.setText(title);
        textArea.setText(description);
        balls.setSelectedItem(ball);
    }

    private static class Parse{
        public static String title(Component component){
            String title = ((JLabel) component).getText();
            title = title.replace("<html>", "").replace("</html>", "");
            return title;
        }

        public static String balls(Component component){
            String balls = ((JLabel) component).getText();
            int liof = balls.lastIndexOf(' ');
            int liol = balls.lastIndexOf('/');
            return balls.substring(liof + 1, liol);
        }

        public static String description(Component component){
            JScrollPane scrollPane = (JScrollPane) component;
            JViewport viewport = (JViewport) scrollPane.getComponent(0);
            Component comp = viewport.getView();
            String description = ((JLabel) comp).getText();
            description = description.replace("<html><div style='width:290px;'>","")
                    .replace("</div></html>", "");
            return description;
        }
    }

}
