package main.java.com.vega.ania.windows.updateContainer;

import main.java.com.vega.ania.windows.addContainer.AddButtons;
import main.java.com.vega.ania.windows.addContainer.AddForm;

import java.awt.*;

/**
 * Created by Вася-Вега on 18.11.2015.
 */
public class UpdateActivity extends Container {
    public static int HEIGHT = 565;
    public static int TOPMARGIN = 5;
    public static int FORMMARGINLEFT = 5;
    public static int BUTTONMARGINLEFT =600;
    public static int FORMWIDTH = 590;
    public static int BUTTIONWIDTH =190;

    public UpdateActivity() {
        add(new UpdateForm(FORMMARGINLEFT, TOPMARGIN, FORMWIDTH, HEIGHT));
        add(new UpdateButtons(BUTTONMARGINLEFT, TOPMARGIN, BUTTIONWIDTH, HEIGHT));
    }
}
