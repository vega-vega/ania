package main.java.com.vega.ania.windows.settingsContainer;

import main.java.com.vega.ania.extro.Setting;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Вася-Вега on 22.11.2015.
 */
public class SettingsForm extends JPanel{

    final static int LABELWIDTH = 215;
    final static int LABELHEIGHT = 20;

    public SettingsForm(int x, int y, int width, int height) {
        setBounds(x, y, width, height);
        setLayout(null);
        JLabel label = new JLabel("PARSE SETTING");
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
        label.setLocation(50, 10);
        label.setSize(LABELWIDTH, LABELHEIGHT);
        add(label);

        label = new JLabel("Parse html");
        label.setLocation(50, 40);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setSize(LABELWIDTH, LABELHEIGHT);
        add(label);

        add(new Parse(45, 70));

        label = new JLabel("Parse DOC");
        label.setLocation(50, 200);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setSize(LABELWIDTH, LABELHEIGHT);
        add(label);

        add(new ParseDOC(45, 230));

        label = new JLabel("MAIN SETTING");
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
        label.setLocation(310, 10);
        label.setSize(LABELWIDTH, LABELHEIGHT);
        add(label);

        label = new JLabel("Show setting");
        label.setLocation(310, 40);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setSize(LABELWIDTH, LABELHEIGHT);
        add(label);

        add(new Show(305, 70));

        label = new JLabel("Balls");
        label.setLocation(310, 160);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setSize(LABELWIDTH, LABELHEIGHT);
        add(label);

        add(new Balls(305, 190));
    }

    public static class Parse extends JPanel{

        final static int WIDTH = 225;
        final static int HEIGHT = 110;
        final static int PADDING = 10;
        final static int PADDINGXINPUT = 65;
        final static int HIGHTITEM = 20;
        final static int WIDTHLABEL = 40;
        final static int WIDTHTEXT = 150;

        public static JTextField imageParse = new JTextField();
        public static JTextField textParse = new JTextField();
        public static JTextField titleParse = new JTextField();

        public Parse(int x, int y) {
            setBounds(x, y, WIDTH, HEIGHT);
            setLayout(null);
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2),
                    BorderFactory.createLineBorder(Color.black)));

            JLabel jLabel = new JLabel("Image:");
            jLabel.setLocation(PADDING, PADDING);
            jLabel.setSize(WIDTHLABEL, HIGHTITEM);
            imageParse.setLocation(PADDINGXINPUT, PADDING);
            imageParse.setSize(WIDTHTEXT, HIGHTITEM);
            imageParse.setText(Setting.getParseImg());
            add(jLabel);
            add(imageParse);

            jLabel = new JLabel("Title:");
            jLabel.setLocation(PADDING, PADDING * 2 + HIGHTITEM);
            jLabel.setSize(WIDTHLABEL, HIGHTITEM);
            titleParse.setLocation(PADDINGXINPUT, PADDING * 2 + HIGHTITEM);
            titleParse.setSize(WIDTHTEXT, HIGHTITEM);
            titleParse.setText(Setting.getParseTitle());
            add(jLabel);
            add(titleParse);

            jLabel = new JLabel("Text:");
            jLabel.setLocation(PADDING, PADDING * 3 + HIGHTITEM * 2);
            jLabel.setSize(WIDTHLABEL, HIGHTITEM);
            textParse.setLocation(PADDINGXINPUT, PADDING * 3 + HIGHTITEM * 2);
            textParse.setSize(WIDTHTEXT, HIGHTITEM);
            textParse.setText(Setting.getParseText());
            add(jLabel);
            add(textParse);
        }
    }

    public static class ParseDOC extends JPanel{

        final static int WIDTH = 225;
        final static int HEIGHT = 110;
        final static int PADDING = 10;
        final static int PADDINGXINPUT = 185;
        final static int HIGHTITEM = 20;
        final static int WIDTHLABEL = 160;
        final static int WIDTHTEXT = 30;

        public static JTextField numParTitle = new JTextField();
        public static JTextField countParMax = new JTextField();
        public static JTextField numParText = new JTextField();

        public ParseDOC(int x, int y) {
            setBounds(x, y, WIDTH, HEIGHT);
            setLayout(null);
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2),
                    BorderFactory.createLineBorder(Color.black)));

            JLabel jLabel = new JLabel("Number title paragraph:");
            jLabel.setLocation(PADDING, PADDING);
            jLabel.setSize(WIDTHLABEL, HIGHTITEM);
            numParTitle.setLocation(PADDINGXINPUT, PADDING);
            numParTitle.setSize(WIDTHTEXT, HIGHTITEM);
            String title = String.valueOf(Setting.getNumParTitle() + 1);
            numParTitle.setText(title);
            add(jLabel);
            add(numParTitle);

            jLabel = new JLabel("Number text paragraph:");
            jLabel.setLocation(PADDING, PADDING * 2 + HIGHTITEM);
            jLabel.setSize(WIDTHLABEL, HIGHTITEM);
            numParText.setLocation(PADDINGXINPUT, PADDING * 2 + HIGHTITEM);
            numParText.setSize(WIDTHTEXT, HIGHTITEM);
            String text = String.valueOf(Setting.getNumParText() + 1);
            numParText.setText(text);
            add(jLabel);
            add(numParText);

            jLabel = new JLabel("Max count paragraph:");
            jLabel.setLocation(PADDING, PADDING * 3 + HIGHTITEM * 2);
            jLabel.setSize(WIDTHLABEL, HIGHTITEM);
            countParMax.setLocation(PADDINGXINPUT, PADDING * 3 + HIGHTITEM * 2);
            countParMax.setSize(WIDTHTEXT, HIGHTITEM);
            String max = String.valueOf(Setting.getCountParMax() + 1);
            countParMax.setText(max);
            add(jLabel);
            add(countParMax);
        }
    }

    public static class Show extends JPanel{

        final static int WIDTH = 225;
        final static int HEIGHT = 75;
        final static int PADDINGX = 30;
        final static int PADDINGY = 10;
        final static int HIGHTITEM = 20;
        final static int WIDTHITEM = 80;

        static JCheckBox showImg = new JCheckBox("image", Setting.isShowImg());
        static JCheckBox showTitle = new JCheckBox("title", Setting.isShowTitle());
        static JCheckBox showText = new JCheckBox("text", Setting.isShowText());
        static JCheckBox showBalls = new JCheckBox("balls", Setting.isShowBalls());

        public Show(int x, int y) {
            setBounds(x, y, WIDTH, HEIGHT);
            setLayout(null);
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2),
                    BorderFactory.createLineBorder(Color.black)));


            showImg.setLocation(PADDINGX, PADDINGY);
            showImg.setSize(WIDTHITEM, HIGHTITEM);
            add(showImg);

            showTitle.setLocation(PADDINGX + WIDTHITEM + 20, PADDINGY);
            showTitle.setSize(WIDTHITEM, HIGHTITEM);
            add(showTitle);

            showText.setLocation(PADDINGX, PADDINGY * 2 + HIGHTITEM);
            showText.setSize(WIDTHITEM, HIGHTITEM);
            add(showText);

            showBalls.setLocation(PADDINGX + WIDTHITEM + 20, PADDINGY * 2 + HIGHTITEM);
            showBalls.setSize(WIDTHITEM, HIGHTITEM);
            add(showBalls);

        }
    }

    public static class Balls extends JPanel{

        final static int WIDTH = 225;
        final static int HEIGHT = 65;
        final static int PADDINGX = 25;
        final static int PADDINGY = 10;
        final static int HIGHTITEM = 20;
        final static int WIDTHITEM = 80;

        static JTextField minBalls = new JTextField();
        static JTextField maxBalls = new JTextField();

        public Balls(int x, int y) {
            setBounds(x, y, WIDTH, HEIGHT);
            setLayout(null);
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2),
                    BorderFactory.createLineBorder(Color.black)));

            JLabel label = new JLabel("min");
            label.setLocation(PADDINGX, PADDINGY);
            label.setSize(WIDTHITEM, HIGHTITEM);
            label.setHorizontalAlignment(SwingConstants.CENTER);
            add(label);

            label = new JLabel("max");
            label.setLocation(PADDINGX + 20 + WIDTHITEM, PADDINGY);
            label.setSize(WIDTHITEM, HIGHTITEM);
            label.setHorizontalAlignment(SwingConstants.CENTER);
            add(label);

            minBalls.setLocation(PADDINGX, PADDINGY + HIGHTITEM);
            minBalls.setSize(WIDTHITEM, HIGHTITEM);
            minBalls.setText(String.valueOf(Setting.getMinBalls()));
            add(minBalls);

            maxBalls.setLocation(PADDINGX + 20 + WIDTHITEM, PADDINGY + HIGHTITEM);
            maxBalls.setSize(WIDTHITEM, HIGHTITEM);
            maxBalls.setText(String.valueOf(Setting.getMaxBalls()));
            add(maxBalls);
        }
    }

}
