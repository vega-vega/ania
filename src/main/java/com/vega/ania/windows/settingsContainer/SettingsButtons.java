package main.java.com.vega.ania.windows.settingsContainer;

import main.java.com.vega.ania.extro.ComponentsList;
import main.java.com.vega.ania.Main;
import main.java.com.vega.ania.SQLite.Database;
import main.java.com.vega.ania.extro.Setting;
import main.java.com.vega.ania.windows.addContainer.AddActivity;
import main.java.com.vega.ania.windows.mainContainer.MainActivity;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;

/**
 * Created by Вася-Вега on 22.11.2015.
 */
public class SettingsButtons extends JPanel {

    private static int LOCATIONWIDTH = 120;
    private static int LOCATIONHEIGHT = 50;
    private static int WIDTH = 100;
    private static int HEIGHT = 30;

    public SettingsButtons(int x, int y, int width, int height) {
        setBounds(x, y, width, height);
        LOCATIONHEIGHT = height / 12;
        LOCATIONWIDTH = width / 6;
        WIDTH = width / 2 + LOCATIONWIDTH;
        setOpaque(false);
        setLayout(null);

        JButton button = new JButton("Save");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 1);
        button.addActionListener(new SaveButton());
        add(button);

        button = new JButton("Delete all");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 2);
        button.addActionListener(new DeleteButton());
        add(button);

        button = new JButton("Close");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 3);
        button.addActionListener(new CloseButton());
        add(button);
    }

    private class SaveButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {

            String parseImg = SettingsForm.Parse.imageParse.getText();
            String parseTitle = SettingsForm.Parse.titleParse.getText();
            String parseText = SettingsForm.Parse.textParse.getText();
            Integer numParTitle = Integer.valueOf(SettingsForm.ParseDOC.numParTitle.getText());
            Integer numParText = Integer.valueOf(SettingsForm.ParseDOC.numParText.getText());
            Integer countParMax = Integer.valueOf(SettingsForm.ParseDOC.countParMax.getText());
            Boolean showImg = SettingsForm.Show.showImg.isSelected();
            Boolean showTitle = SettingsForm.Show.showTitle.isSelected();
            Boolean showText = SettingsForm.Show.showText.isSelected();
            Boolean showBalls = SettingsForm.Show.showBalls.isSelected();
            Integer minBalls = Integer.valueOf(SettingsForm.Balls.minBalls.getText());
            Integer maxBalls = Integer.valueOf(SettingsForm.Balls.maxBalls.getText());

            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("parseimg", parseImg);
            hashMap.put("parsetitle", parseTitle);
            hashMap.put("parsetext", parseText);
            hashMap.put("numpartitle", numParTitle);
            hashMap.put("numpartext", numParText);
            hashMap.put("countparmax", countParMax);
            hashMap.put("showimg", showImg);
            hashMap.put("showtitle", showTitle);
            hashMap.put("showtext", showText);
            hashMap.put("showballs", showBalls);
            hashMap.put("minballs", minBalls);
            hashMap.put("maxballs", maxBalls);
            new Setting(hashMap);

            Database.updateSetting(parseImg, parseTitle, parseText, numParTitle, numParText,
                    countParMax, checkBoolean(showImg), checkBoolean(showTitle),
                    checkBoolean(showText), checkBoolean(showBalls), minBalls, maxBalls);

            Main.jFrame.getContentPane().removeAll();
            Main.jFrame.add(new MainActivity());
            Main.addFrame.getContentPane().removeAll();
            Main.addFrame.add(new AddActivity());
        }
    }

    private class DeleteButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Database.clearPost();
            ComponentsList.clearItems();
            Main.jFrame.getContentPane().removeAll();
            Main.jFrame.add(new MainActivity());
            for (File myFile : new File(Main.PATHIMG).listFiles())
                if (myFile.isFile()) myFile.delete();
        }
    }

    private class CloseButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Main.jFrame.setLocation(Main.settingFrame.getLocation());
            Main.jFrame.setVisible(true);
            Main.settingFrame.setVisible(false);
        }
    }

    private int checkBoolean(boolean b){
        if (b){
            return 1;
        }else {
            return 0;
        }
    }
}
