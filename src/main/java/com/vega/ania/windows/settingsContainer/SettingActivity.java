package main.java.com.vega.ania.windows.settingsContainer;

import java.awt.*;

/**
 * Created by Вася-Вега on 22.11.2015.
 */
public class SettingActivity extends Container{

    public static int HEIGHT = 565;
    public static int TOPMARGIN = 5;
    public static int LISTMARGINLEFT = 5;
    public static int BUTTONMARGINLEFT =600;
    public static int LISTWIDTH = 590;
    public static int BUTTIONWIDTH =190;

    public SettingActivity() {
        add(new SettingsForm(LISTMARGINLEFT, TOPMARGIN, LISTWIDTH, HEIGHT));
        add(new SettingsButtons(BUTTONMARGINLEFT, TOPMARGIN, BUTTIONWIDTH, HEIGHT));
    }
}
