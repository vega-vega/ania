package main.java.com.vega.ania.windows.mainContainer;

import java.awt.*;

/**
 * Created by Вася-Вега on 16.11.2015.
 */
public class MainActivity extends Container{
    public static int HEIGHT = 565;
    public static int TOPMARGIN = 5;
    public static int LISTMARGINLEFT = 5;
    public static int BUTTONMARGINLEFT =600;
    public static int LISTWIDTH = 590;
    public static int BUTTIONWIDTH =190;

    public MainActivity() {
        add(new MainList(LISTMARGINLEFT, TOPMARGIN, LISTWIDTH, HEIGHT));
        add(new MainButtons(BUTTONMARGINLEFT, TOPMARGIN, BUTTIONWIDTH, HEIGHT));
    }

}
