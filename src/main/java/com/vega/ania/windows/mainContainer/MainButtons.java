package main.java.com.vega.ania.windows.mainContainer;

import main.java.com.vega.ania.Main;
import main.java.com.vega.ania.extro.Output;
import main.java.com.vega.ania.extro.Parse;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Вася-Вега on 16.11.2015.
 */
public class MainButtons extends JPanel {

    private static int LOCATIONWIDTH = 120;
    private static int LOCATIONHEIGHT = 50;
    private static int WIDTH = 100;
    private static int HEIGHT = 30;

    static JFileChooser fileChooser;
    static {
        FileFilter newFilter = new FileNameExtensionFilter("TEXT FILES", "txt", "text", "docx", "doc");
        fileChooser = new JFileChooser();
        FileFilter oldFilter = fileChooser.getFileFilter();
        fileChooser.removeChoosableFileFilter(oldFilter);
        fileChooser.setFileFilter(newFilter);
    }


    public MainButtons(int x, int y, int width, int height) {
        setBounds(x, y, width, height);
        LOCATIONHEIGHT = height / 12;
        LOCATIONWIDTH = width / 6;
        WIDTH = width / 2 + LOCATIONWIDTH;
        setOpaque(false);
        setLayout(null);

        JButton button = new JButton("Add");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 1);
        button.addActionListener(new AddButton());
        add(button);

        button = new JButton("Load");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 2);
        button.addActionListener(new LoadButton());
        add(button);

        button = new JButton("Unload");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 3);
        button.addActionListener(new UnloadButton());
        add(button);

        button = new JButton("Settings");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 4);
        button.addActionListener(new SettingsButton());
        add(button);

        button = new JButton("Exit");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 5);
        button.addActionListener(new ExitButton());
        add(button);

        button = new JButton("About");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 11);
        button.addActionListener(new AboutButton());
        add(button);
    }

    private class ExitButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }

    private class AddButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Main.addFrame.setLocation(Main.jFrame.getLocation());
            Main.addFrame.setVisible(true);
            Main.jFrame.setVisible(false);
        }
    }

    private class LoadButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            int result = fileChooser.showDialog(null, "Open file");
            if (result == JFileChooser.APPROVE_OPTION) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Parse.findFile(String.valueOf(fileChooser.getSelectedFile()));
                        Main.jFrame.getContentPane().removeAll();
                        Main.jFrame.add(new MainActivity());
                        Main.jFrame.setVisible(true);
                    }
                });
                t.start();
            }
        }
    }

    private class UnloadButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    Output output = new Output();
                    output.outputDOCX();
                }
            });
            t.start();
        }
    }

    private class SettingsButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Main.settingFrame.setLocation(Main.jFrame.getLocation());
            Main.settingFrame.setVisible(true);
            Main.jFrame.setVisible(false);
        }
    }

    private class AboutButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            new AboutWindow();
        }
    }

    private static class AboutWindow{

        private JPanel panel;
        private JFrame about;

        public AboutWindow() {
            about = new JFrame("About");
            about.setSize(200, 150);
            panel = new JPanel();
            panel.setLayout(new BorderLayout());
            about.add(panel);
            about.setLocationRelativeTo(null);
            about.setResizable(false);
            about.setVisible(true);
            JLabel label = new JLabel("<html>Создатель: Василий Константинович<br> Почта: f2269@mail.ru </html>");
            label.setHorizontalAlignment(SwingConstants.CENTER);
            JButton button = new JButton("Exit");
            button.addActionListener(new Exit());
            panel.add(label, BorderLayout.NORTH);
            panel.add(button, BorderLayout.SOUTH);
            panel.validate();
        }

        private class Exit implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                about.dispose();
            }
        }
    }
}
