package main.java.com.vega.ania.windows.mainContainer;

import main.java.com.vega.ania.Main;
import main.java.com.vega.ania.SQLite.Database;
import main.java.com.vega.ania.extro.ComponentsList;
import main.java.com.vega.ania.extro.DefaultImage;
import main.java.com.vega.ania.extro.Setting;
import main.java.com.vega.ania.extro.SettingImage;
import main.java.com.vega.ania.windows.updateContainer.UpdateForm;

import javax.swing.*;
import javax.swing.event.AncestorListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Map;

/**
 * Created by Вася-Вега on 16.11.2015.
 */
public class MainList extends JPanel{

    private static int count = 0;
    private JPanel jPanel = new JPanel();

    public MainList(int x, int y, int width, int height) {
        setBounds(x, y, width, height);
        setLayout(new BorderLayout());
        setOpaque(false);


        jPanel.setOpaque(false);
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.PAGE_AXIS));
        for (Map.Entry<Integer, ComponentsList.ListItem> listItem : ComponentsList.getComponentsList().entrySet()){
            List<Component> list = listItem.getValue().getListItem();
            jPanel.add(addPost(listItem.getKey(), (JLabel) list.get(0), (JLabel) list.get(1),
                    (JScrollPane) list.get(2), (JLabel) list.get(3)));
        }

        JScrollPane scrollPane = new JScrollPane(jPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        JScrollBar jsp = scrollPane.getVerticalScrollBar();
        jsp.setUnitIncrement(60);
        add(scrollPane);
    }

    private JPanel addPost(Integer id, JLabel title, JLabel img, JScrollPane text, JLabel ballsLabel){
        JPanel panel = new JPanel();
        panel.setBounds(0, getHeight() / 10 * count, this.getWidth(), getHeight() / 4);
        panel.setOpaque(false);
        panel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(2, 5, 3, 5),
                BorderFactory.createLineBorder(Color.black)));

        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();


        title.setPreferredSize(new Dimension(panel.getWidth() - 35, 40));
        title.setVerticalAlignment(SwingConstants.CENTER);
        title.setHorizontalAlignment(SwingConstants.CENTER);
        text.setPreferredSize(new Dimension(panel.getWidth() / 2 + 100, panel.getHeight()));

        c.fill = GridBagConstraints.NONE;


        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        c.anchor = GridBagConstraints.EAST;
        c.insets = new Insets(0, 0, 0, 16);
        JLabel editItem = new JLabel(SettingImage.getUpdateImage());
        editItem.setPreferredSize(new Dimension(15, 15));
        editItem.addMouseListener(new UpdateItem(id));

        JLabel deleteItem = new JLabel((SettingImage.getDeleteImage()));
        deleteItem.setPreferredSize(new Dimension(15,15));
        deleteItem.addMouseListener(new DeleteItem(id));


        panel.add(editItem, c);
        c.insets = new Insets(0, 0, 0, 0);
        panel.add(deleteItem, c);

        c.anchor = GridBagConstraints.CENTER;

        if (Setting.isShowTitle()) {
            c.insets = new Insets(0, 0, 3, 0);
            c.gridx = 0;
            c.gridy = 1;
            c.gridwidth = 2;
            panel.add(title, c);
        }

        if (Setting.isShowImg()) {
            c.insets = new Insets(0, 3, 2, 3);
            c.gridx = 0;
            c.gridy = 2;
            c.gridwidth = 1;
            panel.add(img, c);
        }

        if (Setting.isShowText()) {
            c.insets = new Insets(0, 3, 2, 3);
            c.gridy = 2;
            c.gridx = 1;
            c.gridwidth = 1;
            panel.add(text, c);
        }

        if (Setting.isShowBalls()) {
            c.gridx = 0;
            c.gridy = 3;
            c.gridwidth = 2;
            c.anchor = GridBagConstraints.EAST;
            c.insets = new Insets(0, 0, 0, 0);
            panel.add(ballsLabel, c);
        }

        count++;

        return panel;
    }

    private class UpdateItem implements MouseListener {
        private Integer id;
        public UpdateItem(Integer id) {
            this.id = id;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            UpdateForm.findItem(id);
            Main.updateFrame.setLocation(Main.jFrame.getLocation());
            Main.jFrame.setVisible(false);
            Main.updateFrame.setVisible(true);
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }

    private class DeleteItem implements MouseListener{
        private Integer id;

        public DeleteItem(Integer id) {
            this.id = id;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            Database.deletePost(id);
            ComponentsList.deleteItem(id);
            Main.jFrame.getContentPane().removeAll();
            Main.jFrame.add(new MainActivity());
            Main.jFrame.setVisible(true);
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}
