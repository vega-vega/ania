package main.java.com.vega.ania.windows.addContainer;

import main.java.com.vega.ania.extro.Setting;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Вася-Вега on 18.11.2015.
 */
public class AddForm extends JPanel {

    static JFileChooser fileChooser;
    final static int HEIGHT = 25;
    final static int LEFTMARGIN = 50;
    final static int YLOCATION = 75;
    public static JTextField imgPath  = new JTextField();
    public static JTextField titleText = new JTextField();
    public static JTextArea textArea = new JTextArea();
    public static JComboBox balls = new JComboBox(Setting.getBalls());

    static {
        FileFilter newFilter = new FileNameExtensionFilter("Image", "jpg", "png");
        fileChooser = new JFileChooser();
        FileFilter oldFilter = fileChooser.getFileFilter();
        fileChooser.removeChoosableFileFilter(oldFilter);
        fileChooser.setFileFilter(newFilter);
    }

    public AddForm(int x, int y, int width, int height) {
        setBounds(x, y, width, height);
        setOpaque(false);
        setLayout(null);
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(2, 5, 3, 5), null));

        imgPath.setSize(345, HEIGHT);
        imgPath.setLocation(LEFTMARGIN, YLOCATION);
        add(imgPath);

        JButton button = new JButton("path");
        button.setSize(80, HEIGHT);
        button.setLocation(400, YLOCATION);
        button.addActionListener(new PathButton());
        add(button);

        titleText.setSize(430, HEIGHT);
        titleText.setLocation(LEFTMARGIN, YLOCATION * 2);
        add(titleText);


        textArea.setSize(430, 180);
        textArea.setLocation(LEFTMARGIN, YLOCATION * 3);
        add(textArea);

        balls = new JComboBox(Setting.getBalls());
        balls.setEditable(true);
        balls.setSize(75, HEIGHT);
        balls.setLocation(LEFTMARGIN, YLOCATION * 6);
        add(balls);

        Font font = new Font(Font.SANS_SERIF, 3, 16);

        JLabel label = new JLabel("Path for image");
        label.setSize(430, HEIGHT);
        label.setLocation(LEFTMARGIN, (YLOCATION - 35));
        label.setForeground(Color.orange);
        label.setFont(font);
        add(label);

        label = new JLabel("Title post");
        label.setSize(430, HEIGHT);
        label.setLocation(LEFTMARGIN, YLOCATION * 2 - 35);
        label.setForeground(Color.orange);
        label.setFont(font);
        add(label);

        label = new JLabel("Text in post");
        label.setSize(430, HEIGHT);
        label.setLocation(LEFTMARGIN, YLOCATION * 3 - 35);
        label.setForeground(Color.orange);
        label.setFont(font);
        add(label);

        label = new JLabel("Balls of post");
        label.setSize(430, HEIGHT);
        label.setLocation(LEFTMARGIN, YLOCATION * 6 - 35);
        label.setForeground(Color.orange);
        label.setFont(font);
        add(label);

    }

    private class PathButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            int result = fileChooser.showDialog(null, "Open image");
            if (result == JFileChooser.APPROVE_OPTION){
                imgPath.setText(String.valueOf(fileChooser.getSelectedFile()));
            }
        }
    }
}
