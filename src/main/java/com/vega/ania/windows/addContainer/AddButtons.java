package main.java.com.vega.ania.windows.addContainer;

import main.java.com.vega.ania.extro.ComponentsList;
import main.java.com.vega.ania.Main;
import main.java.com.vega.ania.SQLite.Database;
import main.java.com.vega.ania.extro.ResizeImg;
import main.java.com.vega.ania.windows.mainContainer.MainActivity;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Вася-Вега on 18.11.2015.
 */
public class AddButtons extends JPanel {

    private static int LOCATIONWIDTH = 120;
    private static int LOCATIONHEIGHT = 50;
    private static int WIDTH = 100;
    private static int HEIGHT = 30;
    private JLabel acceptLabel = new JLabel("Add accepted");
    private JLabel deniedLabel = new JLabel("Add denied");

    public AddButtons(int x, int y, int width, int height) {
        setBounds(x, y, width, height);
        LOCATIONHEIGHT = height / 12;
        LOCATIONWIDTH = width / 6;
        WIDTH = width / 2 + LOCATIONWIDTH;
        setOpaque(false);
        setLayout(null);

        JButton button = new JButton("Add");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 1);
        button.addActionListener(new AddButton());
        add(button);

        button = new JButton("Close");
        button.setSize(WIDTH, HEIGHT);
        button.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 2);
        button.addActionListener(new CloseButton());
        add(button);


        acceptLabel.setSize(WIDTH, HEIGHT);
        acceptLabel.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 3);
        acceptLabel.setForeground(Color.green);
        acceptLabel.setFont(new Font(Font.SANS_SERIF, 3, 18));
        acceptLabel.setVisible(false);
        add(acceptLabel);

        deniedLabel.setSize(WIDTH, HEIGHT);
        deniedLabel.setLocation(LOCATIONWIDTH, LOCATIONHEIGHT * 3);
        deniedLabel.setForeground(Color.red);
        deniedLabel.setFont(new Font(Font.SANS_SERIF, 3, 18));
        deniedLabel.setVisible(false);
        add(deniedLabel);
    }

    private class CloseButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Main.jFrame.setLocation(Main.addFrame.getLocation());
            Main.addFrame.setVisible(false);
            Main.jFrame.setVisible(true);
            acceptLabel.setVisible(false);
            deniedLabel.setVisible(false);
        }
    }

    private class AddButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String title = AddForm.titleText.getText();
            String img = ResizeImg.path(AddForm.imgPath.getText());
            String text = AddForm.textArea.getText();
            int balls = Integer.parseInt(AddForm.balls.getSelectedItem().toString());
            if (title.length() > 0 && img.length() > 0 && text.length() > 0) {
                Integer id = Database.writeTable(title, img, text, balls);
                ComponentsList.addItemList(id, title, new ImageIcon(Main.PATHIMG + img),
                        text, String.valueOf(balls));
                Main.jFrame.getContentPane().removeAll();
                Main.jFrame.add(new MainActivity());
                deniedLabel.setVisible(false);
                acceptLabel.setVisible(true);
            }else {
                deniedLabel.setVisible(true);
                acceptLabel.setVisible(false);
            }
        }
    }
}
