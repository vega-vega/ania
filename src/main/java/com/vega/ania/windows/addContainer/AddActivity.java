package main.java.com.vega.ania.windows.addContainer;

import java.awt.*;

/**
 * Created by Вася-Вега on 18.11.2015.
 */
public class AddActivity extends Container {
    public static int HEIGHT = 565;
    public static int TOPMARGIN = 5;
    public static int FORMMARGINLEFT = 5;
    public static int BUTTONMARGINLEFT =600;
    public static int FORMWIDTH = 590;
    public static int BUTTIONWIDTH =190;

    public AddActivity() {
        add(new AddForm(FORMMARGINLEFT, TOPMARGIN, FORMWIDTH, HEIGHT));
        add(new AddButtons(BUTTONMARGINLEFT, TOPMARGIN, BUTTIONWIDTH, HEIGHT));
    }
}
