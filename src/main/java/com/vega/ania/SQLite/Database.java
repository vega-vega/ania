package main.java.com.vega.ania.SQLite;

import main.java.com.vega.ania.extro.Setting;

import java.sql.*;

/**
 * Created by Вася-Вега on 20.11.2015.
 */
public class Database {
    public static Connection connection;
    private static final String CREATETABLE1 = "CREATE TABLE if not exists 'posts' " +
            "('id' INTEGER PRIMARY KEY AUTOINCREMENT, 'title' text, 'img' text, " +
            "'text' text, 'balls' int);";
    private static final String CREATETABLE2 = "CREATE TABLE if not exists 'settings' " +
            "('parseimg' text, 'parsetext' text, 'parsetitle' text, 'numpartitle' int, " +
            "'numpartext' int, 'countparmax' int, " +
            "'showimg' int, 'showtitle' int, 'showtext' int, 'showballs' int, " +
            "'minballs' int, 'maxballs' int);";

    static  {
        try {
            connection = null;
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:aniaBD.s3db");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void createDB(){
        try {
            Statement statement = connection.createStatement();
            statement.execute(CREATETABLE1);
            statement.execute(CREATETABLE2);
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(*) from settings");
            ResultSet resultSet = preparedStatement.executeQuery();
            int count = 0;
            while (resultSet.next()){
                count = resultSet.getInt("COUNT(*)");
            }
            if (count == 0){
                preparedStatement = connection.prepareStatement("insert into settings values (?,?,?,?,?,?,?,?,?,?,?,?)");
                preparedStatement.setString(1, "[itemprop=image]");
                preparedStatement.setString(2, "[itemprop=description]");
                preparedStatement.setString(3, "[itemprop=name]");
                preparedStatement.setInt(4, 1);
                preparedStatement.setInt(5, 0);
                preparedStatement.setInt(6, 1);
                preparedStatement.setInt(7, 1);
                preparedStatement.setInt(8, 1);
                preparedStatement.setInt(9, 1);
                preparedStatement.setInt(10, 1);
                preparedStatement.setInt(11, 0);
                preparedStatement.setInt(12, 5);
                preparedStatement.executeUpdate();
            }
            preparedStatement = connection.prepareStatement("select * from settings");
            new Setting(preparedStatement.executeQuery());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Integer writeTable(String title, String img, String text, int balls){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into posts values (?,?,?,?,?)");
            preparedStatement.setString(2, title);
            preparedStatement.setString(3, img);
            preparedStatement.setString(4, text);
            preparedStatement.setInt(5, balls);
            preparedStatement.executeUpdate();
            ResultSet tableKeys = preparedStatement.getGeneratedKeys();
            tableKeys.next();
            return tableKeys.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ResultSet readTable(){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from posts");
            return preparedStatement.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void clearPost(){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE from posts");
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deletePost(int id){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE from posts WHERE id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updatePost(String title, String img, String text, int balls, int id){
        try {
            PreparedStatement preparedStatement = null;
            if (img == null) {
                preparedStatement = connection.prepareStatement("update posts set title = ?, " +
                        "text = ?, balls = ? WHERE id = ?");
                preparedStatement.setString(1, title);
                preparedStatement.setString(2, text);
                preparedStatement.setInt(3, balls);
                preparedStatement.setInt(4, id);
            }else {
                preparedStatement = connection.prepareStatement("update posts set title = ?, img = ?, " +
                        "text = ?, balls = ? WHERE id = ?");
                preparedStatement.setString(1, title);
                preparedStatement.setString(2, img);
                preparedStatement.setString(3, text);
                preparedStatement.setInt(4, balls);
                preparedStatement.setInt(5, id);
            }
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateSetting(String parseImg, String parseTitle, String parseText,
                                     int numParTitle, int numParText, int countParMax,
                                     int showimg, int showtitle, int showtext,
                                     int showballs, int minballs, int maxballs){
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("update settings set parseimg = ?, parsetext = ?, parsetitle = ?, " +
                            "numpartitle = ?, numpartext = ?, countparmax = ?, showimg = ?, showtitle = ?, " +
                            "showtext = ?, showballs = ?, minballs = ?, maxballs = ?");
            preparedStatement.setString(1, parseImg);
            preparedStatement.setString(2, parseText);
            preparedStatement.setString(3, parseTitle);
            preparedStatement.setInt(4, numParTitle);
            preparedStatement.setInt(5, numParText);
            preparedStatement.setInt(6, countParMax);
            preparedStatement.setInt(7, showimg);
            preparedStatement.setInt(8, showtitle);
            preparedStatement.setInt(9, showtext);
            preparedStatement.setInt(10, showballs);
            preparedStatement.setInt(11, minballs);
            preparedStatement.setInt(12, maxballs);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
