package main.java.com.vega.ania.extro;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * Created by Вася-Вега on 10.12.2015.
 */
public class Output {

    private XWPFDocument newDoc = new XWPFDocument();
    private static String fileName;
    public static boolean isBreak;
    public static boolean isClose;
    public static boolean isDo;

    public void outputDOCX(){
        try {
            isBreak = false;
            isClose = false;
            isDo = false;
            fileName = null;
            Window window = new Window();
            while (isClose == false) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (isDo) {
                int maxCount = ComponentsList.getComponentsList().size();
                int doneCount = 0;
                for (ComponentsList.ListItem listItem : ComponentsList.getComponentsList().values()) {
                    java.util.List<Component> list = listItem.getListItem();
                    String title = Parse.title(list.get(0));
                    InputStream image = Parse.image(list.get(1));
                    String description = Parse.description(list.get(2));
                    String balls = ((JLabel) list.get(3)).getText();
                    createDOCX(title, image, description, balls);
                    window.showProgress(maxCount,doneCount++);
                    if (isBreak) break;
                }
                newDoc.createParagraph();
                FileOutputStream fos = new FileOutputStream(fileName);
                newDoc.write(fos);
                fos.flush();
                fos.close();
            }
            window.done();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createDOCX(String title, InputStream image, String description, String balls) throws IOException {
        try {
            XWPFParagraph paragraph = newDoc.createParagraph();
            XWPFRun run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            run.setBold(true);
            run.setText(title);

            paragraph = newDoc.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            run.addPicture(image, XWPFDocument.PICTURE_TYPE_JPEG, "", Units.toEMU(150), Units.toEMU(150));

            paragraph = newDoc.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            run.setText(description);

            paragraph = newDoc.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            run.setText(balls);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    private static class Parse{
        public static String title(Component component){
            String title = ((JLabel) component).getText();
            title = title.replace("<html>", "").replace("</html>", "");
            return title;
        }

        public static InputStream image(Component component) throws IOException {
            Icon icon = ((JLabel) component).getIcon();
            BufferedImage bi = new BufferedImage( icon.getIconWidth(),
                    icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics2D g2d = (Graphics2D) bi.getGraphics();
            icon.paintIcon(null, g2d, 0, 0);
            g2d.dispose();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(bi, "jpeg", os);
            InputStream is = new ByteArrayInputStream(os.toByteArray());
            return is;
        }

        public static String description(Component component){
            JScrollPane scrollPane = (JScrollPane) component;
            JViewport viewport = (JViewport) scrollPane.getComponent(0);
            Component comp = viewport.getView();
            String description = ((JLabel) comp).getText();
            description = description.replace("<html><div style='width:290px;'>","")
                    .replace("</div></html>", "");
            return description;
        }
    }

    private static class Window{

        private JPanel panel;
        private static JFrame process;
        private static JTextArea textArea;

        public Window() {
            process= new JFrame("Unload");
            process.setSize(200, 90);
            panel = new JPanel();
            panel.setLayout(new BorderLayout());
            process.add(panel);
            process.setLocationRelativeTo(null);
            process.setResizable(false);
            process.setVisible(true);
            process.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            onCreate();
        }

        private void onCreate(){
            JLabel title = new JLabel("Укажите имя файла");
            title.setHorizontalAlignment(SwingConstants.CENTER);
            textArea = new JTextArea();
            JButton closeBut = new JButton("Close");
            closeBut.addActionListener(new CloseButton());
            JButton okBut = new JButton("Ok");
            okBut.addActionListener(new OkButton());

            panel.removeAll();
            panel.add(title, BorderLayout.NORTH);
            panel.add(textArea, BorderLayout.CENTER);
            panel.add(okBut, BorderLayout.EAST);
            panel.add(closeBut, BorderLayout.SOUTH);
            panel.validate();
        }

        public void showProgress(int maxCount, int doneCount){
            JLabel label = new JLabel("Done: " + ++doneCount + " / " + maxCount);
            label.setHorizontalAlignment(SwingConstants.CENTER);
            JButton button = new JButton("Stop");
            button.addActionListener(new StopLoad());
            panel.removeAll();
            panel.add(label, BorderLayout.NORTH);
            panel.add(button, BorderLayout.SOUTH);
            panel.validate();
        }

        public void done(){
            process.dispose();
        }

        private static class StopLoad implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                isBreak = true;
            }
        }

        private static class OkButton implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = textArea.getText();
                if (name.length() > 0){
                    isDo = true;
                    isClose = true;
                    name = name.replaceAll("[:\\\\/*?|<>]", "_");
                    fileName = name + ".docx";
                }
            }
        }
        private static class CloseButton implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                isClose = true;
                process.dispose();
            }
        }
    }

}
