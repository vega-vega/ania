package main.java.com.vega.ania.extro;

import main.java.com.vega.ania.Main;
import main.java.com.vega.ania.SQLite.Database;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.model.PicturesTable;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFPicture;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * Created by Вася-Вега on 02.12.2015.
 */
public class Parse {
    private static File file;
    private static boolean isBreak = false;
    private static int balls = 2;

    public static void findFile(String url){
        int index = url.lastIndexOf(".");
        String type = url.substring(index);

        if(".txt".equals(type)) {
            parseTXT(url);
        }
        if (".docx".equals(type)){
            parseDOCX(url);
        }
        if(".doc".equals(type)){
            parseDOC(url);
        }

    }

    private static void parseDOC(String url){
        try {
            POIFSFileSystem pfs = new POIFSFileSystem(new FileInputStream(url));
            HWPFDocument doc = new HWPFDocument(pfs);
            Range range = doc.getRange();
            PicturesTable picT = doc.getPicturesTable();
            java.util.List picL = picT.getAllPictures();
            ProgressWindow window = new ProgressWindow();
            int maxCount = range.numParagraphs();
            int doneCount = 0;
            int step = 0;
            int countPar = 0;
            String title = null;
            String text = null;
            for (int i = 0; i < range.numParagraphs(); i++) {
                window.showProgress(maxCount, doneCount++);
                Paragraph par = range.getParagraph(i);
                String checkNull = par.text().replaceAll("\\s", "");
                if(checkNull.length() > 1){
                    if (countPar == Setting.getNumParTitle()){
                        title = par.text();
                    }else if (countPar == Setting.getNumParText() &&
                            Setting.getNumParText() > 0){
                        text = par.text();
                    }
                    if (++countPar >= Setting.getCountParMax()) {
                        if (title != null) {
                            Picture picture = (Picture) picL.get(step++);
                            BufferedImage image1 = ImageIO.read(new ByteArrayInputStream(picture.getContent()));
                            String img = ResizeImg.docx(image1);
                            Integer id = Database.writeTable(title, img, text, balls);
                            ComponentsList.addItemList(id, title, new ImageIcon(Main.PATHIMG + img),
                                    text, String.valueOf(balls));
                            title = null;
                            text = null;
                        }
                        countPar = 0;
                    }
                }
            }
            window.done();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void parseDOCX(String url){
        try {
            XWPFDocument doc = new XWPFDocument(new FileInputStream(url));
            int countPar = 0;
            String title = null;
            String text = null;
            BufferedImage image = null;
            ProgressWindow window = new ProgressWindow();
            int maxCount = doc.getParagraphs().size();
            int doneCount = 0;
            for (XWPFParagraph p : doc.getParagraphs()) {
                window.showProgress(maxCount, doneCount++);
                String checkNull = p.getText().replaceAll("\\s", "");

                for (XWPFRun run : p.getRuns()) {
                    for (XWPFPicture pic : run.getEmbeddedPictures()) {
                        byte[] img = pic.getPictureData().getData();
                        image = ImageIO.read(new ByteArrayInputStream(img));
                    }
                }

                if (countPar >= Setting.getCountParMax()) {
                    if (title != null && image != null) {
                        String img = ResizeImg.docx(image);
                        Integer id = Database.writeTable(title, img, text, balls);
                        ComponentsList.addItemList(id, title, new ImageIcon(Main.PATHIMG + img),
                                text, String.valueOf(balls));
                        title = null;
                        text = null;
                        image = null;
                        countPar = 0;
                    }
                }

                if (checkNull.length() > 1) {
                    String paragraph = p.getText();

                    if (countPar == Setting.getNumParTitle()) {
                        title = paragraph;
                    } else if (countPar == Setting.getNumParText() &&
                            Setting.getNumParText() > 0) {
                        text = paragraph;
                    }
                    countPar++;
                }
            }
            window.done();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private static void parseTXT(String url){
        int maxCount = 0;
        int doneCount = 0;
        try {
            file = new File(url);
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = br.readLine()) != null) {
                maxCount++;
            }
            ProgressWindow window = new ProgressWindow();
            br = new BufferedReader(new FileReader(file));
            line = null;
            while ((line = br.readLine()) != null) {
                window.showProgress(maxCount, doneCount++);
                parseHTML(line);
                if (isBreak) break;
            }
            isBreak = false;
            br.close();
            window.done();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void parseHTML(String url){
        try {
            org.jsoup.nodes.Document document = Jsoup.connect(url)
                    .userAgent("").get();
            Elements elements = document.select(Setting.getParseTitle());
            String title = elements.first().ownText();
            elements = document.select(Setting.getParseImg());
            String img = ResizeImg.url(elements.first().absUrl("src"));
            elements = document.select(Setting.getParseText());
            String text = elements.first().ownText();
            if (title.length() > 0 && img.length() > 0 && text.length() > 0) {
                Integer id = Database.writeTable(title, img, text, balls);
                ComponentsList.addItemList(id, title, new ImageIcon(Main.PATHIMG + img),
                        text, String.valueOf(balls));
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private static class ProgressWindow{

        private JPanel panel;
        private JFrame process;

        public ProgressWindow() {
            process= new JFrame("Load");
            process.setSize(200, 150);
            panel = new JPanel();
            panel.setLayout(new BorderLayout());
            process.add(panel);
            process.setLocationRelativeTo(null);
            process.setResizable(false);
            process.setVisible(true);
        }

        public void showProgress(int maxCount, int doneCount){
            JLabel label = new JLabel("Done: " + ++doneCount + " / " + maxCount);
            label.setHorizontalAlignment(SwingConstants.CENTER);
            JButton button = new JButton("Stop");
            button.addActionListener(new StopLoad());
            panel.removeAll();
            panel.add(label, BorderLayout.NORTH);
            panel.add(button, BorderLayout.SOUTH);
            panel.validate();
        }

        public void done(){
            process.dispose();
        }

        private static class StopLoad implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                isBreak = true;
            }
        }
    }
}
