package main.java.com.vega.ania.extro;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by Вася-Вега on 25.11.2015.
 */
public class Setting {
    static String parseImg = "";
    static String parseText = "";
    static String parseTitle = "";
    static int numParTitle = 0;
    static int numParText = 0;
    static int countParMax = 0;
    static boolean showImg = false;
    static boolean showTitle = false;
    static boolean showText = false;
    static boolean showBalls = false;
    static int minBalls = 0;
    static int maxBalls = 5;

    public Setting(ResultSet resultSet) {
        try {
            while (resultSet.next()){
                parseImg = resultSet.getString("parseimg");
                parseText = resultSet.getString("parsetext");
                parseTitle = resultSet.getString("parsetitle");
                numParTitle = resultSet.getInt("numpartitle");
                numParText = resultSet.getInt("numpartext");
                countParMax = resultSet.getInt("countparmax");
                showImg = checkInt(resultSet.getInt("showimg"));
                showTitle = checkInt(resultSet.getInt("showtitle"));
                showText = checkInt(resultSet.getInt("showtext"));
                showBalls = checkInt(resultSet.getInt("showballs"));
                minBalls = resultSet.getInt("minballs");
                maxBalls = resultSet.getInt("maxballs");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Setting(HashMap hashMap){
        parseImg = (String) hashMap.get("parseimg");
        parseText = (String) hashMap.get("parsetext");
        parseTitle = (String) hashMap.get("parsetitle");
        numParTitle = (Integer) hashMap.get("numpartitle");
        numParText = (Integer) hashMap.get("numpartext");
        countParMax = (Integer) hashMap.get("countparmax");
        showImg = (Boolean) hashMap.get("showimg");
        showTitle = (Boolean) hashMap.get("showtitle");
        showText = (Boolean) hashMap.get("showtext");
        showBalls = (Boolean) hashMap.get("showballs");
        minBalls = (Integer) hashMap.get("minballs");
        maxBalls = (Integer) hashMap.get("maxballs");
    }

    public static String getParseImg() {
        return parseImg;
    }

    public static String getParseText() {
        return parseText;
    }

    public static String getParseTitle() { return parseTitle; }

    public static boolean isShowImg() {
        return showImg;
    }

    public static boolean isShowTitle() {
        return showTitle;
    }

    public static boolean isShowText() {
        return showText;
    }

    public static boolean isShowBalls() {
        return showBalls;
    }

    public static int getMinBalls() {
        return minBalls;
    }

    public static int getMaxBalls() {
        return maxBalls;
    }

    public static int getNumParTitle() { return numParTitle - 1; }

    public static int getNumParText() { return numParText - 1; }

    public static int getCountParMax() { return countParMax - 1; }

    public static String[] getBalls(){
        String[] balls = new String[maxBalls - minBalls + 1];
        int countBalls = 0;
        for (int i = minBalls; i <= maxBalls; i++, countBalls++){
            balls[countBalls] = String.valueOf(i);
        }
        return balls;
    }

    private boolean checkInt(int i){
        if(i == 1){
            return true;
        }else {
            return false;
        }
    }
}
