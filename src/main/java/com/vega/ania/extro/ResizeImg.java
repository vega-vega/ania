package main.java.com.vega.ania.extro;

import main.java.com.vega.ania.Main;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

/**
 * Created by Вася-Вега on 07.12.2015.
 */
public class ResizeImg {

    private final static int HEIGHTIMG = 175;
    private final static int WIDTHIMG = 150;

    public static String path(String path){
        Image image = new ImageIcon(path).getImage();
        return saveImg(path, '\\', image);
    }

    public static String url(String path) throws IOException {
        URL url = new URL(path);
        URLConnection conn = url.openConnection();
        conn.setRequestProperty("User-Agent", "");
        Image image1 = ImageIO.read(conn.getInputStream());
        Image image = new ImageIcon(image1).getImage();
        return saveImg(path, '/', image);
    }

    public static String docx(BufferedImage bufferedImage){
        Image image = bufferedImage;
        Random random = new Random();
        String name = "/" + String.valueOf(random.nextInt()) + ".";
        return saveImg(name, '/', image);
    }

    public static String doc(){
        Random random = new Random();
        String name = "/" + String.valueOf(random.nextInt()) + ".";
        return null;
    }

    private static String saveImg(String path, char divider, Image image){
        BufferedImage resizeImage = new BufferedImage(WIDTHIMG, HEIGHTIMG, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = resizeImage.createGraphics();
        g.drawImage(image, 0, 0, WIDTHIMG, HEIGHTIMG, null);
        g.dispose();
        int liof = path.lastIndexOf(divider);
        int liol = path.lastIndexOf('.');

        if (liof != -1 && liol != -1) {
            String fileName = path.substring(liof + 1, liol) + ".png";
            try {
                ImageIO.write(resizeImage, "png", new File(Main.PATHIMG + fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return fileName;
        }
        return "";
    }
}
