package main.java.com.vega.ania.extro;

import main.java.com.vega.ania.Main;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Вася-Вега on 26.11.2015.
 */
public class ComponentsList {

    private static HashMap<Integer, ListItem> componentsList = new HashMap<>();

    public ComponentsList() {
    }

    public static void addItemList(Integer id, String titleString, ImageIcon imageIcon, String textString, String balls) {
        JLabel title = new JLabel("<html>" + titleString + "</html>");
        JLabel img = new JLabel(imageIcon);
        JLabel text = new JLabel("<html><div style='width:290px;'>" + textString + "</div></html>");
        JScrollPane scrollText = new JScrollPane(text, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollText.setPreferredSize(new Dimension(0, 150));
        scrollText.setBorder(null);
        JLabel ballsLabel = new JLabel("balls: " + balls + "/" + Setting.getMaxBalls());
        componentsList.put(id,  new ListItem(title, img, scrollText, ballsLabel));
    }

    public static HashMap<Integer, ListItem> getComponentsList() {
        return componentsList;
    }

    public static void clearItems(){
        componentsList = new HashMap<>();
    }

    public static void deleteItem(Integer id){
        componentsList.remove(id);
    }

    public static ListItem getItem(Integer id){
        return componentsList.get(id);
    }

    public static void updateItem(Integer id, String titleString, String image, String textString, String balls){
        JLabel title = new JLabel("<html>" + titleString + "</html>");
        JLabel img = null;
        if (image == null){
            java.util.List<Component> list = getItem(id).getListItem();
            img = (JLabel) list.get(1);
        }else {
            img = new JLabel(new ImageIcon(Main.PATHIMG + image));
        }
        JLabel text = new JLabel("<html><div style='width:290px;'>" + textString + "</div></html>");
        JScrollPane scrollText = new JScrollPane(text, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollText.setPreferredSize(new Dimension(0, 150));
        scrollText.setBorder(null);
        JLabel ballsLabel = new JLabel("balls: " + balls + "/" + Setting.getMaxBalls());
        componentsList.put(id,  new ListItem(title, img, scrollText, ballsLabel));
    }

    public static class ListItem{

        private List<Component> listItem = new ArrayList<>();

        public ListItem(JLabel title, JLabel img, JScrollPane text, JLabel balls) {
            listItem.add(title);
            listItem.add(img);
            listItem.add(text);
            listItem.add(balls);
        }

        public List<Component> getListItem() {
            return listItem;
        }
    }
}
